import path from "path";
import { dirname } from "path";
import { fileURLToPath } from "url";

import HtmlBundlerPlugin from "html-bundler-webpack-plugin";

const __dirname = dirname(fileURLToPath(import.meta.url));

const commonConfig = {

    module: {
        rules: [
            {
                test: /\.(ico|png|jp?g|svg)/,
                type: "asset",
                generator: { filename: "img/[name].[hash:8][ext]" },
                parser: { dataUrlCondition: { maxSize: 2 * 1024 } }
            }
        ]
    },

    output: {
        path: path.resolve(__dirname, "dist"),
        clean: true
    },

    plugins: [
        new HtmlBundlerPlugin({
            entry: { index: "./example/index.html" },
            js: { filename: "[name].[contenthash:8].js" },
            css: { filename: "[name].[contenthash:8].css" }
        })
    ],

    resolve: {
        alias: {
          "@styles": path.join(__dirname, "src/style")
        },
      }

 };

 export { commonConfig };
 export default commonConfig;