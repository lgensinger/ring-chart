import { select } from "d3-selection";
import { arc } from "d3-shape";
import { transition } from "d3-transition";

import { ChartLabel, PieLayout as PL, RadialGrid } from "@lgv/visualization-chart";

import { configuration, configurationLayout } from "../configuration.js";

/**
 * RingChart is a part-of-a-whole visualization.
 * @param {array} data - objects where each represents a path in the hierarchy
 * @param {integer} height - artboard height
 * @param {float} innerRadius - value between 0 and 1 as percentage of total circle to make inner radius
 * @param {integer} width - artboard width
 */
class RingChart extends RadialGrid {
    constructor(data, width=configurationLayout.width, height=configurationLayout.height, PieLayout=null, innerRadius=0.5, label=configuration.branding, name=configuration.name) {

        // initialize inheritance
        super(data, width, height, PieLayout ? PieLayout : new PL(data, { height: height, inner: innerRadius, width: width }), label, name);

        // update self
        this.arc = null;
        this.classArc = `${label}-arc`;
        this.classLabel = `${label}-label`;
        this.classLabelPartial = `${label}-label-partial`;
        this.innerRadius = innerRadius;
        this.label = null;
        this.labelPartial = null;

    }

    /**
     * Construct arc for slices.
     * @returns A d3 arc function.
     */
    get arcs() {
        return arc()
            .outerRadius(this.radius)
            .innerRadius(this.radius * this.innerRadius);
    }

    /**
     * Position and minimally style arcs in SVG dom element.
     */
    configureArcEvents() {
        this.arc
            .on("click", (e,d) => this.configureEvent("arc-click",d,e))
            .on("mouseover", (e,d) => {

                // update class
                select(e.target).attr("class", `${this.classArc} active`);

                // send event to parent
                this.configureEvent("arc-mouseover",d,e);

            })
            .on("mouseout", (e,d) => {

                // update class
                select(e.target).attr("class", this.classArc);

                // send event to parent
                this.artboard.dispatch("arc-mouseout", {
                    bubbles: true
                });

            });
    }

    /**
     * Position and minimally style arcs in SVG dom element.
     */
    configureArcs() {
        this.arc
            .transition().duration(1000)
            .attr("class", this.classArc)
            .attr("data-id", d => this.Data.extractId(d))
            .attr("data-label", d => this.Data.extractLabel(d))
            .attr("data-arc-value", d => this.Data.extractValue(d))
            .attr("fill", "transparent")
            .attr("stroke", "currentColor")
            .attr("d", d => this.arcs(d));
    }

    /**
     * Position and minimally style label partials in SVG dom element.
     */
    configureLabelPartials() {
        this.labelPartial
            .transition().duration(300)
            .attr("class", this.classLabelPartial)
            .attr("x", 0)
            .attr("dy", (d,i) => this.Label.calculateDy(i, d, this.arcs.outerRadius()(d) - this.arcs.innerRadius()(d)))
            .textTween((d,i) => this.tweenText(this.Data.extractLabel(d), this.Data.extractValue(d), this.arcs.outerRadius()(d) - this.arcs.innerRadius()(d), i));
    }

    /**
     * Position and minimally style labels in SVG dom element.
     */
    configureLabels() {
        this.label
            .transition().duration(1000)
            .attr("class", this.classLabel)
            .attr("data-angle-orthant", d => this.orthant(d.degree))
            .attr("data-arc-value", d => this.Data.extractValue(d))
            .attr("pointer-events", "none")
            .attr("transform", d => `translate(${this.arcs.centroid(d)[0]}, ${this.arcs.centroid(d)[1]})`)
            .attr("text-anchor", "middle");
    }

    /**
     * Generate arcs in SVG elements.
     * @param {selection} selection - d3.js selection
     * @returns A d3.js selection.
     */
    generateArcs(selection) {
        return selection
            .selectAll(`.${this.classArc}`)
            .data(this.data)
            .join(
                enter => enter.append("path"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Generate visualization.
     */
    generateChart() {

        // initialize labeling
        this.Label = new ChartLabel(this.artboard, this.Data);

        // generate arcs
        this.arc = this.generateArcs(this.content);
        this.configureArcs();
        this.configureArcEvents();

        // generate labels
        this.label = this.generateLabels(this.content);
        this.configureLabels();

        // generate label partials so they stack
        this.labelPartial = this.generateLabelPartials(this.label);
        this.configureLabelPartials();

    }

    /**
     * Generate arc label partials in SVG element.
     *  @param {selection} selection - d3.js SVG selection
     * @returns A d3.js selection.
     */
    generateLabelPartials(selection) {
        return selection
            .selectAll(`.${this.classLabelPartial}`)
            .data(d => [d, d])
            .join(
                enter => enter.append("tspan"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Generate arc value labels in SVG element.
     * @param {selection} selection - d3.js SVG selection
     * * @returns A d3.js selection.
     */
    generateLabels(selection) {
        return selection
            .selectAll(`.${this.classLabel}`)
            .data(this.data)
            .join(
                enter => enter.append("text"),
                update => update,
                exit => exit.remove()
            );
    }

};

export { RingChart };
export default RingChart;
