import { configuration as config, configurationLayout as configLayout } from "@lgv/visualization-chart";
import packagejson from "../package.json";

const configuration = {
    branding: process.env.LGV_BRANDING || config.branding,
    name: packagejson.name.replace("/", "-").slice(1)
};

const configurationLayout = {
    height: process.env.LGV_HEIGHT || configLayout.height,
    width: process.env.LGV_WIDTH || configLayout.width
}

export { configuration, configurationLayout };
export default configuration;